package proyecto;

public class Simulador {
	
	private int total_dias;
	private Comunidad comunidad;
	
	Simulador(int total_dias, Comunidad comunidad){
		
		this.total_dias = total_dias;
		this.comunidad = comunidad;
	}
	
	void pasar_de_los_dias() {
		
		for(int p=0; p<total_dias; p++) {
			System.out.println("El dia " + (p+1) + " hay:");
			
			if( p == 0) {
				comunidad.generarCiudadanos();
		        comunidad.infectadosIniciales(5);
		        comunidad.casos_activos();
		        comunidad.recuperarse();
		        comunidad.total_infectados();
		        
				
			}
			else {
			   comunidad.contacto_fisico();
		       comunidad.verificar_estado();
			   comunidad.casos_activos();
			   comunidad.recuperarse();
			   comunidad.total_infectados();

			}
		System.out.println("*****************************");
		}
	}
	

}
