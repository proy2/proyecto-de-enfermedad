# PROYECTO 1: "SIMULACION DE UNA ENFERMEDAD INFECCIOSA "

_El proyecto consiste en la simulacion de una enfermedad infecciosa, donde se analiza la trazabilidad de esta enfermedad en una poblacion, utilizando la programacion orientada a objetos._


# EXPLICACION: 

Dentro de una comunidad, los ciudadanos se ven expuestos a una enfermedad, en la simulacion se determina el comportamiento de el virus y el estado de la comunidad, determinando los contactos estrechos, los contagiaos y los recuperados, asi como la probabilidad de conexion fisica y la probabilidad de infeccion, por medio de ciclos se visualiza la propagacion de la enfermedad en medida de "steps" que representan el grado de infeccion de la cepa,en esta se van actualizando los datos de los ciudadanos recuperados o muertos, para luego entregar los datos finales sobre el estado de la comunidad, los casos totales de infectados (total de personas que han sido infectadas, recuperadas y enfermas ) 
y los cantagios totales de la comunidad. 


**Pre-requisitos**

- Sistema operativo linux 
- Java 
- IDE para el lenguaje Java


> Instalacion  

**PARA EJECUTAR LA SIMULACION:**
1. Descargue todos los archivos del siguiente repositorio
2. Abra su editor de texto y dirijase al lugar donde están ubicados los archivos que acaba de descargar
3. Espere algunos segundos para que se ejecute el programa
4. Si quiere utilizar diferentes valores para la comunidad, los dias de recuperacion, el numero de habitantes o el numero de conexion fisica se pueden modificar dentro de la clase Main 



**POO en el programa:** ejemplo:

`Ciudadano(int id_, Comunidad comunidad, Enfermedad enfermedad){

	        this.id_ = id_;
	        this.comunidad = comunidad;
	        this.enfermedad = enfermedad;

	        // Estos valores son predeterminados para todos los ciudadanos.
	        this.recuperado = false;
	        this.enfermo = false;
	        this.contacto_fisico = false;
	    }`



**Creado con:**
- Java 
- Eclipse IDE 2021-03
- Eclipse IDE 2019-12








**Autoras:**


- Patricia Fuentes (PatriciaFuentesG)


- Paz Echeverría - (pazjecheverriaf)








 






