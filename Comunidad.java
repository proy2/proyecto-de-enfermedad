package proyecto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Comunidad {
	
	private int i,p ,total_infectados;
	private int numero_habitantes;
    private int promedio_con_fisica;
    private ArrayList<Ciudadano> personas;
    private ArrayList<Ciudadano> cont_fisico;
    private Enfermedad enfermedad;
    private double probabilidad_con_fisica;
    Random rd = new Random();

    Comunidad (int numero_habitantes, int promedio_con_fisica, double probabilidad_con_fisica, Enfermedad enfermedad){

        this.numero_habitantes = numero_habitantes;
        this.promedio_con_fisica = promedio_con_fisica;
        this.probabilidad_con_fisica = probabilidad_con_fisica;
        this.enfermedad = enfermedad;
        this.personas  = new ArrayList<Ciudadano>();
        this.cont_fisico = new ArrayList<Ciudadano>();    
       }

    public void generarCiudadanos(){

        // Se crean ciudadanos y se meten a la lista.
        for(int i=0; i<this.numero_habitantes; i++){
            this.personas.add(new Ciudadano(i, this, this.enfermedad));
        }
    }

    public void infectadosIniciales(int iniciales){

        // Creacion de infectados iniciales
        Collections.shuffle(this.personas);

        for(int i=0; i<iniciales; i++){
            this.personas.get(i).setEnfermo(true);
            }
        }
    
    public void contacto_fisico() {
    	
    	this.cont_fisico.clear();
    	for (Ciudadano c: this.personas){
    		
            if (c.getEnfermo()== true ) {
            	
            	for (int i = 0; i < promedio_con_fisica; i++) {
            		
            		int n = (int)(Math.random()*numero_habitantes);
            		this.cont_fisico.add(this.personas.get(n));
            		}
            	   }
             }
     }    	
     
   public void casos_activos() {
	   
	   i = 0;
	   for (Ciudadano c: this.personas){
		   
           if (c.getEnfermo()== true ) {
        	   i = i+1;
        	   
	          }
           
           } 
	   System.out.println("Total casos activos: " + i);
	   
   }
   
   public void total_infectados() {
       total_infectados = 0;	 
	   for (Ciudadano c: this.personas){
           if (c.getEnfermo()== true || c.getRecuperado() == true) {
        	  
        	   total_infectados = total_infectados + 1;        	   
           }
          }
	   System.out.println("Total casos infectados: " + total_infectados);
	   
	   
   }
   
   public void recuperarse() {
	   
	   for (Ciudadano c: this.personas){
		   if(c.getEnfermo() == true) {
			   			  
			   if (c.getdias_enfermo() == enfermedad.getDias_recuperacion()) {
				   
				   c.setEnfermo(false);
				   c.setRecuperado(true);
			   } else {
				   
				  c.setdias_enfermo();  
			   }   
			  }
		    }
	   }
	   
	   
	   
   
    
   public void verificar_estado() { 
    	

   	for (Ciudadano c: this.cont_fisico){   
    	if ( c.getEnfermo() == false && c.getRecuperado() == false){
    		if(this.rd.nextDouble() < this.probabilidad_con_fisica){
    			if(this.rd.nextDouble() < enfermedad.getInfeccion()){
    				c.setEnfermo(true);
              }
    	}
      }
    }
  }
}    

    	
