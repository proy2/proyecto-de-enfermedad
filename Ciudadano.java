package proyecto;

public class Ciudadano{
	
	    private int id_;
	    private int contador;
	    private boolean recuperado;
	    private boolean enfermo;
	    private boolean contacto_fisico;
	    private Enfermedad enfermedad;
	    private Comunidad comunidad;
	    private int dias_enfermo;

	    Ciudadano(int id_, Comunidad comunidad, Enfermedad enfermedad){

	        this.id_ = id_;
	        this.comunidad = comunidad;
	        this.enfermedad = enfermedad;

	        // Estos valores son predeterminados para todos los ciudadanos.
	        this.recuperado = false;
	        this.enfermo = false;
	        this.contacto_fisico = false;
	    }


	    public void setEnfermo(boolean enfermo){
	    	
	        this.enfermo = enfermo;
	    }

	    public boolean getEnfermo(){
	    	
	        return this.enfermo;
	    }

	    public int getId(){
	    	
	        return this.id_;
	    }
	    
	    public void setRecuperado(boolean recuperado) {
	    	
	    	this.recuperado = recuperado;
	    	
	    }
	    
	    public boolean getRecuperado() {
	    	
	    	return this.recuperado;
	    }

	    
        public void setdias_enfermo() {
	    	
	    	this.dias_enfermo += 1;
	    	
	    }
	    
	    public int getdias_enfermo() {
	    	
	    	return dias_enfermo;

        }
}
